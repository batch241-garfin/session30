

// 2.

db.fruits.aggregate([
{
	$match: {onSale: true}
},
{
	$group: {_id: "$onSale", fruitsOnSale: {$sum: 1}}
},
{
	$project: {_id : 0}
}]);

// 3.

db.fruits.aggregate([
{
	$match: {stock: {$gte: 20}}
},
{
	$group: {_id: "stock", fruitsOnSale: {$sum: 1}}
},
{
	$project: {_id : 0}
}]);

// 4.

db.fruits.aggregate([
{
	$match: {onSale: true}
},
{
	$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}
}]);

// 5.

db.fruits.aggregate([
{
	$match: {onSale: true}
},
{
	$group: {_id: "$supplier_id", max_price: {$max: "$price"}}
},
{
	$sort: {total: 1}
}]);

// 6.

db.fruits.aggregate([
{
	$match: {onSale: true}
},
{
	$group: {_id: "$supplier_id", min_price: {$min: "$price"}}
},
{
	$sort: {total: -1}
}]);